#include <SPI.h>

// PIN definitions
const int altaveu = 6;
const int microfon = 2;
const int ledBlau = 9;
const int ledVermell = 5;
const int ledVerd = 10;
const int sensorLlum = 4;
const int accelerometreX = 5;
const int accelerometreY = 11;
const int accelerometreZ = 6;
//const int intExtern = 3;

// Constants
const int note[] = {
  262, // C
  277, // C#
  294, // D
  311, // D#
  330, // E
  349, // F
  370, // F#
  392, // G
  415, // G#
  440, // A
  466, // A#
  494, // B
  523  // C next octave
};

// Variables
bool senyalant = false;
int previousAccX = 320;
int previousAccY = 350;
int previousAccZ = 440;

void setup() {
  // Settejem els pins
  pinMode(altaveu, OUTPUT);
  pinMode(microfon, INPUT);
  pinMode(ledBlau, OUTPUT);
  pinMode(ledVermell, OUTPUT);
  pinMode(ledVerd, OUTPUT);
  pinMode(sensorLlum, INPUT);
  pinMode(accelerometreX, INPUT);
  pinMode(accelerometreY, INPUT);
  pinMode(accelerometreZ, INPUT);
  //pinMode(intExtern, ?);

  // Desactivem les sortides
  digitalWrite(ledBlau, LOW);
  digitalWrite(ledVermell, LOW);
  digitalWrite(ledVerd, LOW);
  digitalWrite(altaveu, LOW);

  // Inicialitzem el port sèrie
  Serial.begin(9600);
}

void loop() {
  if (!senyalant && vibracioExcessiva()) {
    emetSenyal();
  }
  //delay(3000);
  if (!senyalant && canviLlum()) {
    emetSenyal();
  }
  //delay(3000);
  if (!senyalant && deteccioSoroll()) {
    emetSenyal();
  }
  //delay(3000);
}

bool deteccioSoroll() {
  int valueMic = analogRead(microfon);
  //Serial.println("VALUE MIC:");
  //Serial.println(valueMic);
  return false;
}

bool canviLlum() {
  int valueLl = analogRead(sensorLlum);
  //Serial.println("VALUE LLUM:");
  //Serial.println(valueLl);
  return false;
}

bool vibracioExcessiva() {
  int valueX = analogRead(accelerometreX);
  int valueY = analogRead(accelerometreY);
  int valueZ = analogRead(accelerometreZ);
  if (valueX >= (previousAccX - 4) && valueX <= (previousAccX + 4)) {
    if (valueY >= (previousAccY - 4) && valueY <= (previousAccY + 4)) {
      if (valueZ >= (previousAccZ - 4) && valueZ <= (previousAccZ + 4)) {
        previousAccX = valueX;
        previousAccY = valueY;
        previousAccZ = valueZ;
        return false;
      }
    }
  }
  previousAccX = valueX;
  previousAccY = valueY;
  previousAccZ = valueZ;
  return true;
}

void emetSenyal() {
  senyalant = true;
  emetSenyalAcustica();
  mostraSenyalLluminosa();
  delay(100);
  aturaSenyalLluminosa();
  aturaSenyalAcustica();
  senyalant = false;
}

void emetSenyalAcustica() {
   int i = 0;
   while (i < 1000000) {
    digitalWrite(altaveu, HIGH);
    delayMicroseconds(100); // Approximately 10% duty cycle @ 1KHz
    digitalWrite(altaveu, LOW);
    delayMicroseconds(1000 - 100);
    i++;
   }
}

void aturaSenyalAcustica() {
  //noTone(altaveu);
}

void mostraSenyalLluminosa() {
  digitalWrite(ledVermell, HIGH);
  delay(100);
  digitalWrite(ledVerd, HIGH);
  delay(100);
  digitalWrite(ledBlau, HIGH);
}

void aturaSenyalLluminosa() {
  digitalWrite(ledBlau, LOW);
  delay(100);
  digitalWrite(ledVerd, LOW);
  delay(100);
  digitalWrite(ledVermell, LOW);
}
